﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;


namespace ClaseBase
{
    public class TrabajarUsuario
    {
        

        public Usuario obtenerUsuario(string user, string pass)
        {
            Usuario usuario = new Usuario();
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT * FROM Usuario WHERE usu_nombre = @user AND usu_password= @pass", conexion);
            comando.Parameters.AddWithValue("user", user);
            comando.Parameters.AddWithValue("pass",pass);
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            conexion.Close();
            if (dataTable.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                usuario.Usu_id = (int)dataTable.Rows[0]["usu_id"];
                usuario.Usu_nombre = dataTable.Rows[0]["usu_nombre"].ToString();
                usuario.Usu_password = dataTable.Rows[0]["usu_password"].ToString();
                usuario.Usu_apellidoNombre = dataTable.Rows[0]["usu_apellidoNombre"].ToString();
                usuario.Rol_codigo = dataTable.Rows[0]["rol_codigo"].ToString();
                return usuario;

            }
        }

        public ObservableCollection<Usuario> traerUsuario()
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            ObservableCollection<Usuario> listaUsuario = new ObservableCollection<Usuario>();
            
            SqlCommand comando = new SqlCommand();
            comando.CommandText = "SELECT * FROM Usuario";
            comando.CommandType = CommandType.Text;
            comando.Connection = conexion;
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            if (dataTable.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    Usuario usuario = new Usuario();
                    usuario.Usu_id = (int)row["usu_id"];
                    usuario.Usu_nombre = row["usu_nombre"].ToString();
                    usuario.Usu_password = row["usu_password"].ToString();
                    usuario.Usu_apellidoNombre = row["usu_apellidoNombre"].ToString();
                    usuario.Rol_codigo = row["rol_codigo"].ToString();
                    listaUsuario.Add(usuario);
                    
                }
            }
            return listaUsuario;
        }

        public Usuario buscarUsuarioNombre(String nombre)
        {
            Usuario usuario = new Usuario();
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT * FROM Usuario WHERE usu_nombre = @us", conexion);
            comando.Parameters.AddWithValue("@us", nombre);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            conexion.Close();
            if (dataTable.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                 usuario.Usu_id = (int)dataTable.Rows[0]["usu_id"];
                 usuario.Usu_nombre = dataTable.Rows[0]["usu_nombre"].ToString();
                 usuario.Usu_password = dataTable.Rows[0]["usu_password"].ToString();
                 usuario.Usu_apellidoNombre = dataTable.Rows[0]["usu_apellidoNombre"].ToString();
                 usuario.Rol_codigo = dataTable.Rows[0]["rol_codigo"].ToString();

                
            }
            return usuario;
        }

        public Usuario buscarUsuarioId(int id)
        {
            Usuario usuario = new Usuario();
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand("SELECT * FROM Usuario WHERE usu_id = @id", conexion);
            comando.Parameters.AddWithValue("id", id);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            conexion.Close();
            if (dataTable.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                usuario.Usu_id = (int)dataTable.Rows[0]["usu_id"];
                usuario.Usu_nombre = dataTable.Rows[0]["usu_nombre"].ToString();
                usuario.Usu_password = dataTable.Rows[0]["usu_password"].ToString();
                usuario.Usu_apellidoNombre = dataTable.Rows[0]["usu_apellidoNombre"].ToString();
                usuario.Rol_codigo = dataTable.Rows[0]["rol_codigo"].ToString();

                
            }
            return usuario;
        }

        public void AltaUsuario(Usuario usuario)
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand("INSERT INTO Usuario (usu_id, usu_nombre, usu_password, usu_apellidoNombre, rol_codigo) values (@id, @name, @pass, @nombre, @rol)", conexion);
            comando.Parameters.AddWithValue("@id", usuario.Usu_id);
            comando.Parameters.AddWithValue("@name", usuario.Usu_nombre);
            comando.Parameters.AddWithValue("@pass", usuario.Usu_password);
            comando.Parameters.AddWithValue("@nombre", usuario.Usu_apellidoNombre);
            comando.Parameters.AddWithValue("@rol", usuario.Rol_codigo);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public void BajaUsuario(int id)
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.CommandText = "DELETE FROM Usuario WHERE usu_id = '" + id + "'";
            comando.CommandType = CommandType.Text;
            comando.Connection = conexion;
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public void ModificacionUsuario(Usuario usuario)
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            
            SqlCommand comando = new SqlCommand("UPDATE Usuario SET usu_nombre = @name , usu_password = @pass , usu_apellidoNombre = @nombre , rol_codigo = @rol WHERE usu_id = '"+usuario.Usu_id+"'", conexion);
     
            comando.Parameters.AddWithValue("@name", usuario.Usu_nombre);
            comando.Parameters.AddWithValue("@pass", usuario.Usu_password);
            comando.Parameters.AddWithValue("@nombre", usuario.Usu_apellidoNombre);
            comando.Parameters.AddWithValue("@rol", usuario.Rol_codigo);
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public Usuario validarLogin()
        {
            Usuario usuario = new Usuario();
            usuario.Usu_nombre = "";
            usuario.Usu_password = "";
            return usuario;
        }
    }
}

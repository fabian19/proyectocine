﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaseBase
{
    public class Cliente
    {
        private int cli_dni;
        private string cli_nombre;
        private string cli_apellido;
        private string cli_telefono;
        private string cli_email;

        public Cliente(int dni, string apellido, string nombre, string telefono, string email) 
        {
            cli_dni = dni;
            cli_apellido = apellido;
            cli_nombre = nombre;
            cli_telefono = telefono;
            cli_email = email;
        }

        public int Cli_dni
        {
            get { return cli_dni; }
            set { cli_dni = value; }
        }
        

        public string Cli_nombre
        {
            get { return cli_nombre; }
            set { cli_nombre = value; }
        }
        

        public string Cli_apellido
        {
            get { return cli_apellido; }
            set { cli_apellido = value; }
        }
        

        public string Cli_telefono
        {
            get { return cli_telefono; }
            set { cli_telefono = value; }
        }
        

        public string Cli_email
        {
            get { return cli_email; }
            set { cli_email = value; }
        }
    }
}

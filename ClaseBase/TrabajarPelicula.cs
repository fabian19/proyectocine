﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClaseBase
{
    public class TrabajarPelicula
    {
        static SqlConnection cn = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
        public static DataTable traerPeliculas()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Pelicula";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        public static void insertar_pelicula(Pelicula newPelicula)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO Pelicula(peli_clase,peli_codigo,peli_duracion,peli_genero,peli_titulo) values(@peli_Clase,@peli_Codigo,@peli_Duracion,@peli_Genero,@peli_Titulo)";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@peli_Clase", newPelicula.Peli_clase);
            cmd.Parameters.AddWithValue("@peli_Codigo", newPelicula.Peli_codigo);
            cmd.Parameters.AddWithValue("@peli_Duracion", newPelicula.Peli_duracion);
            cmd.Parameters.AddWithValue("@peli_Genero", newPelicula.Peli_genero);
            cmd.Parameters.AddWithValue("@peli_Titulo", newPelicula.Peli_titulo);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void BajaPelicula(string titulo)
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.CommandText = "DELETE FROM Pelicula WHERE peli_titulo = '" + titulo + "'";
            comando.CommandType = CommandType.Text;
            comando.Connection = conexion;
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public void ModificacionPelicula(Pelicula pelicula)
        {
            SqlConnection conexion = new SqlConnection(ClaseBase.Properties.Settings.Default.conexion);

            SqlCommand comando = new SqlCommand("UPDATE Pelicula SET peli_titulo = @titulo , peli_duracion = @duracion , peli_genero = @genero , peli_clase = @clase WHERE peli_codigo = '" + pelicula.Peli_codigo + "'", conexion);

            comando.Parameters.AddWithValue("@titulo", pelicula.Peli_titulo);
            comando.Parameters.AddWithValue("@duracion", pelicula.Peli_duracion);
            comando.Parameters.AddWithValue("@genero", pelicula.Peli_genero);
            comando.Parameters.AddWithValue("@clase", pelicula.Peli_clase);
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaseBase
{
    public class Proyeccion
    {
        private int proy_codigo;

        public int Proy_codigo
        {
            get { return proy_codigo; }
            set { proy_codigo = value; }
        }
        private string proy_fecha;

        public string Proy_fecha
        {
            get { return proy_fecha; }
            set { proy_fecha = value; }
        }
        private string proy_hora;

        public string Proy_hora
        {
            get { return proy_hora; }
            set { proy_hora = value; }
        }
        private string proy_nroSala;

        public string Proy_nroSala
        {
            get { return proy_nroSala; }
            set { proy_nroSala = value; }
        }
        private string peli_codigo;

        public string Peli_codigo
        {
            get { return peli_codigo; }
            set { peli_codigo = value; }
        }
    }
}

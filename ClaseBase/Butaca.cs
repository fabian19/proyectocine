﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaseBase
{
    public class Butaca
    {
        private string but_fila;
        private string but_nro;
        private string but_nroSala;

        public Butaca(string butFila, string butNro, string nroSala) 
        {
            but_fila = butFila;
            but_nro = butNro;
            but_nroSala = nroSala;
        }

        public string But_fila
        {
            get { return but_fila; }
            set { but_fila = value; }
        }
        

        public string But_nro
        {
            get { return but_nro; }
            set { but_nro = value; }
        }
        

        public string But_nroSala
        {
            get { return but_nroSala; }
            set { but_nroSala = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ClaseBase
{
    public class Usuario : INotifyPropertyChanged , IDataErrorInfo
    {
        private int usu_id;
        private string usu_nombre;
        private string usu_password;
        private string usu_apellidoNombre;
        private string rol_codigo;
        public event PropertyChangedEventHandler PropertyChanged;

        public Usuario()
        {
        }

        public Usuario(int id, string nombre, string password, string apellidoNombre, string rol)
        {
            usu_id = id;
            usu_nombre = nombre;
            usu_password = password;
            usu_apellidoNombre = apellidoNombre;
            rol_codigo = rol;
        }

        public void Notificar(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string error = null;
                switch (columnName)
                {
                    case "Usu_nombre":
                        error = validarNombre();
                        break;
                    case "Usu_password":
                        error = validarPassword();
                        break;
                    case "Usu_apellidoNombre":
                        error = validarApellidoNombre();
                        break;
                    case "Rol_codigo":
                        error = validarRol();
                        break;
                }
                return error;
            }
        }

        private string validarNombre()
        {
            if (String.IsNullOrEmpty(Usu_nombre))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarPassword()
        {
            if (String.IsNullOrEmpty(Usu_password))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarApellidoNombre()
        {
            if (String.IsNullOrEmpty(Usu_apellidoNombre))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarRol()
        {
            if (String.IsNullOrEmpty(Rol_codigo))
            {
                return "el valor del caompo es obligatorio";
            }
            return null;
        }

        

        public int Usu_id
        {
            get { return usu_id; }
            set { usu_id = value; }
        }
        

        public string Usu_nombre
        {
            get { return usu_nombre; }
            set { usu_nombre = value; }
        }
        

        public string Usu_password
        {
            get { return usu_password; }
            set { usu_password = value; }
        }
        

        public string Usu_apellidoNombre
        {
            get { return usu_apellidoNombre; }
            set { usu_apellidoNombre = value; }
        }
        

        public string Rol_codigo
        {
            get { return rol_codigo; }
            set { rol_codigo = value; }
        }

        
    }
}

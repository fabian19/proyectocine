﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaseBase
{
    public class Pelicula
    {
        private string peli_codigo;
        private string peli_titulo;
        private string peli_duracion;
        private string peli_genero;
        private string peli_clase;

        public Pelicula(string codidgo, string titulo, string duracion, string genero, string clase)
        {
            peli_codigo = codidgo;
            peli_titulo = titulo;
            peli_duracion = duracion;
            peli_genero = genero;
            peli_clase = clase;
        }

        public string Peli_codigo
        {
            get { return peli_codigo; }
            set { peli_codigo = value; }
        }
        

        public string Peli_titulo
        {
            get { return peli_titulo; }
            set { peli_titulo = value; }
        }
        

        public string Peli_duracion
        {
            get { return peli_duracion; }
            set { peli_duracion = value; }
        }
        

        public string Peli_genero
        {
            get { return peli_genero; }
            set { peli_genero = value; }
        }
        

        public string Peli_clase
        {
            get { return peli_clase; }
            set { peli_clase = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClaseBase
{
    public class Ticket
    {
        private int tick_nro;
        private DateTime tick_fechaVenta;
        private int cli_dni;
        private int proy_codigo;
        private string but_fila;
        private string but_nro;

        public Ticket(int nroTick, DateTime fecha, int dni, int codigo, string fila, string nroBut) 
        {
            tick_nro = nroTick;
            tick_fechaVenta = fecha;
            cli_dni = dni;
            proy_codigo = codigo;
            but_fila = fila;
            but_nro = nroBut;
        }

        public int Tick_nro
        {
            get { return tick_nro; }
            set { tick_nro = value; }
        }
        

        public DateTime Tick_fechaVenta
        {
            get { return tick_fechaVenta; }
            set { tick_fechaVenta = value; }
        }
        

        public int Cli_dni
        {
            get { return cli_dni; }
            set { cli_dni = value; }
        }
        

        public int Proy_codigo
        {
            get { return proy_codigo; }
            set { proy_codigo = value; }
        }
        

        public string But_fila
        {
            get { return but_fila; }
            set { but_fila = value; }
        }
        

        public string But_nro
        {
            get { return but_nro; }
            set { but_nro = value; }
        }
    }
}

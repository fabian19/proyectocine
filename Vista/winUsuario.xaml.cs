﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ClaseBase;

namespace Vista
{
    /// <summary>
    /// Interaction logic for winUsuario.xaml
    /// </summary>
    public partial class winUsuario : Window
    {
        TrabajarUsuario trabajarUsuario = new TrabajarUsuario();
        CollectionView Vista;
        ObservableCollection<Usuario> listaUsuarios;

        public winUsuario()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ObjectDataProvider odp = (ObjectDataProvider)this.Resources["LIST_USUARIO"];
            listaUsuarios = odp.Data as ObservableCollection<Usuario>;
            Vista = (CollectionView)CollectionViewSource.GetDefaultView(content.DataContext);
        }

        private void btnAlta_Click(object sender, RoutedEventArgs e)
        {
            if (esCampoVacio(groupBox1))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Seguro que desea guardar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        trabajarUsuario.AltaUsuario(setearObjeto());
                        Usuario usr = new Usuario();
                        usr = setearObjeto();
                        listaUsuarios.Add(usr);
                        MessageBox.Show("Registro guardado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                        Vista.MoveCurrentToLast();
                        limpiarCampos();
                        break;
                }

            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnAlta.Visibility = Visibility.Visible;
            limpiarCampos();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if (esCampoVacio(groupBox1))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Usuario usr = new Usuario();
                usr = trabajarUsuario.buscarUsuarioNombre(txtUsuario.Text);//retorna usuario si es que existe//
                if (usr != null)
                {
                    if (usr.Usu_id != int.Parse(txtId.Text))
                    {
                        MessageBox.Show("El nombre usuario: " + txtUsuario.Text + "\n YA EXISTE!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show("Seguro que desea Modificar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                        switch (result)
                        {
                            case MessageBoxResult.OK:
                                Usuario u = new Usuario();
                                u = setearObjeto();
                                Usuario item = listaUsuarios.FirstOrDefault(i => i.Usu_id == int.Parse(txtId.Text));
                                if (item != null)
                                {
                                    item.Usu_apellidoNombre = u.Usu_apellidoNombre;
                                    item.Usu_nombre = u.Usu_nombre;
                                    item.Usu_password = u.Usu_password;
                                    item.Rol_codigo = u.Rol_codigo;
                                }
                                trabajarUsuario.ModificacionUsuario(item);
                                btnAtras_Click(sender, e);
                                btnSiguiente_Click(sender, e);
                                MessageBox.Show("Registro modificado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                                limpiarCampos();
                                break;
                        }
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("Seguro que desea Modificar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            Usuario u = new Usuario();
                            u = setearObjeto();
                            Usuario item = listaUsuarios.FirstOrDefault(i => i.Usu_id == int.Parse(txtId.Text));
                            if (item != null)
                            {
                                item.Usu_apellidoNombre = u.Usu_apellidoNombre;
                                item.Usu_nombre = u.Usu_nombre;
                                item.Usu_password = u.Usu_password;
                                item.Rol_codigo = u.Rol_codigo;
                            }
                            trabajarUsuario.ModificacionUsuario(item);
                            btnAtras_Click(sender, e);
                            btnSiguiente_Click(sender, e);
                            MessageBox.Show("Registro modificado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                            limpiarCampos();
                            break;
                    }
                }
            }
        }

        private void btnBaja_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que desea ELIMINAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    listaUsuarios.Remove(listaUsuarios.SingleOrDefault(i => i.Usu_id == int.Parse(blockId.Text)));
                    trabajarUsuario.BajaUsuario(int.Parse(blockId.Text));
                    MessageBox.Show("Registro ELIMINADO!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                    btnAtras_Click(sender, e);
                    break;
            }
        }
        private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
            btnAlta.Visibility = Visibility.Hidden;
            btnModificar.Visibility = Visibility.Visible;
            Usuario u = new Usuario();
            u = trabajarUsuario.buscarUsuarioId(int.Parse(blockId.Text));
            txtId.Text = u.Usu_id.ToString();
            txtUsuario.Text = u.Usu_nombre;
            txtPassword.Text = u.Usu_password;
            txtName.Text = u.Usu_apellidoNombre;
            txtRol.Text = u.Rol_codigo;
            //button_Guardar.Visibility = Visibility.Hidden;
        }

        private void btnAtras_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToPrevious();
            if (Vista.IsCurrentBeforeFirst)
            {
                Vista.MoveCurrentToLast();
            }
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToNext();
            if (Vista.IsCurrentAfterLast)
            {
                Vista.MoveCurrentToFirst();
            }
        }

        private void btnPrimero_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToFirst();
        }

        private void btnUltimo_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToLast();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            winUsuarioLista window = new winUsuarioLista();
            window.ShowDialog();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// ----------------FUNCIONES INTERNAS-----------
        /// </summary>
        /// <returns></returns>

        private Usuario setearObjeto() 
        {
            Usuario user = new Usuario();
            user.Usu_id = int.Parse(txtId.Text);
            user.Usu_nombre = txtUsuario.Text;
            user.Usu_password = txtPassword.Text;
            user.Usu_apellidoNombre = txtName.Text;
            user.Rol_codigo = txtRol.Text;
            return user;
        }

        private bool esCampoVacio(GroupBox gp)
        {
            bool vacio = false;
            foreach (Control oControls in ((Grid)gp.Content).Children)
            {
                if (oControls is TextBox)
                {
                    if (((TextBox)oControls).Text == "")
                    {
                        vacio = true;
                    }
                }
            }
            return vacio;
        }

        public void limpiarCampos()
        {
            txtId.Text = "";
            txtUsuario.Text = "";
            txtPassword.Text = "";
            txtName.Text = "";
            txtRol.Text = "";
        }


    }
}

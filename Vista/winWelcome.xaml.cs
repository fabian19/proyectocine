﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaseBase;

namespace Vista
{
    /// <summary>
    /// Interaction logic for winWelcome.xaml
    /// </summary>
    public partial class winWelcome : Window
    {
        winMain main = new winMain();
        TrabajarUsuario trabajarUsuario = new TrabajarUsuario();

        public winWelcome()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            
            String user = login.Usuario;
            String pass = login.Password;
            if (trabajarUsuario.obtenerUsuario(user, pass) != null)
            {
                
                main.UsuarioLog = trabajarUsuario.obtenerUsuario(user, pass);
                MessageBox.Show(main.UsuarioLog.Rol_codigo);
                main.ShowDialog();
            }
            else
            {
                MessageBox.Show("Complete los campos");
            }
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}

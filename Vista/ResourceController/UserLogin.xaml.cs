﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vista.ResourceController
{
    /// <summary>
    /// Interaction logic for UserLogin.xaml
    /// </summary>
    public partial class UserLogin : UserControl
    {
        public UserLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Obtenemos los datos del text y el password
        /// </summary>

        public String Usuario //nombre de usuario
        {
            get { return txtUserName.Text; }
        }

        public String Password //contraseña de usuario
        {
            get { return txtUserPass.Password; }
        }
    }
}

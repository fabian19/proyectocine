﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaseBase;

namespace Vista
{
    /// <summary>
    /// Interaction logic for winPelicula.xaml
    /// </summary>
    public partial class winPelicula : Window
    {
        public winPelicula()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if ((txtTitulo.Text != "") && (txtDuracion.Text != "") && (cmbClase.Text != "") && (cmbGenero.Text != ""))
            {
                MessageBoxResult result = MessageBox.Show("¿Desea agregar pelicula?", "Agregar Pelicula", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    Pelicula pelicula = new Pelicula("1", txtTitulo.Text, txtDuracion.Text, cmbClase.Text, cmbGenero.Text);
                    MessageBox.Show("Se agregó correctamente \n \n" + "titulo: " + pelicula.Peli_titulo + "\n" + "Duración: " + pelicula.Peli_duracion + "min \n" + "género: " + pelicula.Peli_clase + "\n" + "clase: " + pelicula.Peli_genero + "\n");

                    TrabajarPelicula.insertar_pelicula(pelicula);
                    /** Bug: no se recarga la lista */
                    // this.listView1.DataContext = TrabajarPelicula.traerPeliculas(); // recarga los datos con los guardados en base de datos
                    txtTitulo.Text = "";
                    txtDuracion.Text = "";
                    cmbClase.Text = "";
                    cmbGenero.Text = "";
                }
                else
                {

                }
            }
            else
            {
                MessageBox.Show("Ingrese todos los campos");
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClaseBase;

namespace Vista
{
    /// <summary>
    /// Interaction logic for winMain.xaml
    /// </summary>
    public partial class winMain : Window
    {
        private Usuario usuarioLog;

        public winMain()
        {
            InitializeComponent();
            
        }

        
        private void itemUsuario_Click(object sender, RoutedEventArgs e)
        {
            winUsuario window = new winUsuario();
            window.ShowDialog();
        }

        private void itemPelicula_Click(object sender, RoutedEventArgs e)
        {
            winPelicula window = new winPelicula();
            window.ShowDialog();
        }

        private void itemProyeccion_Click(object sender, RoutedEventArgs e)
        {
            winProyecciones window = new winProyecciones();
            window.ShowDialog();
        }

        private void itemButaca_Click(object sender, RoutedEventArgs e)
        {
            winButaca window = new winButaca();
            window.ShowDialog();
        }

        private void itemCliente_Click(object sender, RoutedEventArgs e)
        {
            winCliente window = new winCliente();
            window.ShowDialog();
        }

        private void itemTicket_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            itemUsuario.Visibility = System.Windows.Visibility.Hidden;
            itemCliente.Visibility = System.Windows.Visibility.Hidden;
            itemPelicula.Visibility = System.Windows.Visibility.Hidden;
            itemProyeccion.Visibility = System.Windows.Visibility.Hidden;
            itemTicket.Visibility = System.Windows.Visibility.Hidden;
            itemButaca.Visibility = System.Windows.Visibility.Hidden;

            controlarRol();
        }

        private void controlarRol()
        {
            if (this.usuarioLog.Rol_codigo.Equals("administrador"))
            {
                itemUsuario.Visibility = System.Windows.Visibility.Visible;
                itemPelicula.Visibility = System.Windows.Visibility.Visible;
                itemProyeccion.Visibility = System.Windows.Visibility.Visible;
                itemButaca.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                if (this.usuarioLog.Rol_codigo.Equals("vendedor"))
                {
                    itemCliente.Visibility = System.Windows.Visibility.Visible;
                    itemTicket.Visibility = System.Windows.Visibility.Visible;
                }

            }
        }

        public Usuario UsuarioLog
        {
            get { return usuarioLog; }
            set { usuarioLog = value; }
        }

        
    }
}
